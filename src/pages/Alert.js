import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import {  useSnackbar } from 'notistack';

//import {removeAlert} from '../actions/alert';
// import MuiAlert from '@material-ui/lab/Alert';

const Alert = ({alert}) =>{
  const { enqueueSnackbar } = useSnackbar();

  if(alert.length > 0){
      alert.map((alt)=>  enqueueSnackbar(alt.msg,{variant:`${alt.alertType}`}))
  }
  return <div></div>
  
}

// const { enqueueSnackbar } = useSnackbar();

//   if (errors.length > 0) {
//     debugger
//      enqueueSnackbar(errors[0].msg,{variant:'error'});

//       setTimeout(function(){ 
//         removeAlert( errors[0].id )
//       }, 3000);
//   }


  // else if (loginErrors.length > 0 ) {
    
  //     enqueueSnackbar(loginErrors[0].msg, { variant: 'error' });

  //     setTimeout(function () {
  //         removeAlert(errors[0].id);
  //     }, 3000);
  // }
  // else if(user){
    
  //   enqueueSnackbar('Login Successful', { variant: 'success' });
  // }

  // else if(payload.success){
  //   enqueueSnackbar('Register Successful', { variant: 'success' });
  // }




  

const mapStateToProps= state => ({
 alert : state.alert
})

Alert.propTypes = {
   alert:PropTypes.array.isRequired,
  
};


export default connect(mapStateToProps, null)(Alert);

