import React from "react";
import { Route, Switch } from "react-router-dom";

import Dashboard from "../../Layout/Dashboard";
import Overview from "../../Views/Overview";

import PrivateRoute from "../routing/PrivateRoute";

const Roustes = () => {
	return (
		<section className="container">
			<Switch>
				<PrivateRoute exact path="/dashboard" component={Dashboard} view={Overview} />	
			</Switch>
		</section>
	);
};

export default Roustes;
