import React from "react";
import { Link as RouterLink, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { makeStyles } from "@material-ui/styles";
import {
	Card,
	CardContent,
	CardMedia,
	Typography,
	Divider,
	Link,
	Avatar,
} from "@material-ui/core";
import LockIcon from "@material-ui/icons/Lock";

import Page from "../../components/Page";
import gradients from "../../utils/gradients";
import LoginForm from "./LoginForm";
import authImg from "../../img/auth.png"

const useStyles = makeStyles((theme) => ({
	root: {
		height: "100%",
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		padding: theme.spacing(6, 2),
	},
	card: {
		width: theme.breakpoints.values.md,
		maxWidth: "100%",
		overflow: "unset",
		display: "flex",
		position: "relative",
		"& > *": {
			flexGrow: 1,
			flexBasis: "50%",
			width: "50%",
		},
	},
	content: {
		padding: theme.spacing(8, 4, 3, 4),
	},
	media: {
		borderTopRightRadius: 4,
		borderBottomRightRadius: 4,
		padding: theme.spacing(3),
		color: theme.palette.white,
		display: "flex",
		flexDirection: "column",
		justifyContent: "center",
		[theme.breakpoints.down("md")]: {
			display: "none",
		},
	},
	icon: {
		backgroundImage: gradients.green,
		color: theme.palette.white,
		borderRadius: theme.shape.borderRadius,
		padding: theme.spacing(1),
		position: "absolute",
		top: -32,
		left: theme.spacing(3),
		height: 64,
		width: 64,
		fontSize: 32,
	},
	loginForm: {
		marginTop: theme.spacing(3),
	},
	divider: {
		margin: theme.spacing(2, 0),
	},
	person: {
		marginTop: theme.spacing(2),
		display: "flex",
	},
	avatar: {
		marginRight: theme.spacing(2),
	},
	mediaContent: {
		color: theme.palette.white,
	},
}));

const Landing = ({ isAuthenticated }) => {
	const classes = useStyles();
	if (isAuthenticated) {
		return <Redirect to="/dashboard" />;
	}

	return (
		<Page className={classes.root} title="Login">
			<Card className={classes.card}>
				<CardContent className={classes.content}>
					<LockIcon className={classes.icon} />
					<Typography gutterBottom variant="h3">
						Sign in
					</Typography>
					<LoginForm className={classes.loginForm} />
				</CardContent>
				<CardMedia
					className={classes.media}
					image={authImg}
					title="Cover"
				>
					<Typography
						gutterBottom
						variant="h3"
						align="center"
						className={classes.mediaContent}
					>
						Welcome To Salnaka Admin Panel
					</Typography>
				</CardMedia>
			</Card>
		</Page>
	);
};

Landing.propTypes = {
	isAuthenticated: PropTypes.bool,
};

const mapStateToProps = (state) => ({
	isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps)(Landing);
