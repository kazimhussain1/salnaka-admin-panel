/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
// import validate from 'validate.js';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/styles';
import { Button, TextField } from '@material-ui/core';

import useRouter from '../../../utils/useRouter';
import { login } from '../../../actions/auth';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  root: {},
  fields: {
    margin: theme.spacing(-1),
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      flexGrow: 1,
      margin: theme.spacing(1)
    }
  },
  submitButton: {
    marginTop: theme.spacing(2),
    width: '100%'
  }
}));

const LoginForm = ({ login, isAuthenticated }) => {
  const classes = useStyles();
  const [formData, setFormData] = useState({
    email: '',
    password: ''
  });

  const { email, password } = formData;

  const onChange = e =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = async e => {
    e.preventDefault();
    login(email, password);
  };

  // Redirect if logged in
  if (isAuthenticated) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <form
      className={clsx(classes.root)}
      onSubmit={e => onSubmit(e)}>
      <div className={classes.fields}>
        <TextField
          fullWidth
          helperText=""
          label="Username"
          name="email"
          onChange={e => onChange(e)}
          value={email}
          variant="outlined"
        />
        <TextField
          fullWidth
          helperText=""
          label="Password"
          name="password"
          onChange={e => onChange(e)}
          type="password"
          value={password}
          variant="outlined"
        />
      </div>
      <Button
        className={classes.submitButton}
        color="secondary"
        // disabled={!formState.isValid}
        size="large"
        type="submit"
        variant="contained">
        Sign in
      </Button>
    </form>
  );
};

LoginForm.propTypes = {
  login: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps, { login })(LoginForm);
