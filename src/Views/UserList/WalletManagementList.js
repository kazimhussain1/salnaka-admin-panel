import React, {
  useEffect,
  useState
} from 'react';
import {
  makeStyles
} from '@material-ui/styles';
import { Link as RouterLink } from "react-router-dom";
import clsx from "clsx";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import {
  Button,
  IconButton,
	Card,
	CardActions,
	CardContent,
	CardHeader,
	Divider,
	Table,
	TableBody,
	TableCell,
	TableHead,
	TableRow,
	TableSortLabel,
	Tooltip,
	colors,
} from "@material-ui/core";
import axios from '../../utils/axios'
import { Forward } from '@material-ui/icons';



const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  results: {
    marginTop: theme.spacing(3)
  }
}));

const WalletManagementList = () => {
    const classes = useStyles();
    const [wallets, setWallets] = useState([]);

    useEffect(() => {
      let mounted = true;

      const fetchOrders = async () => {
        try {
          const response = await axios.get(`${process.env.REACT_APP_AXIOS_BASE_URL}/wallet`)
          if (mounted) {
            
            setWallets(response.data.success);
          }
        } catch (error) {
          console.log(error)
        }
      }

      fetchOrders();

      return () => {
        mounted = false;
      };
    }, []);

    return (
      <Table>
								<TableHead>
									<TableRow>
										{/* <TableCell sortDirection="desc">
											<Tooltip enterDelay={300} title="Sort">
												<TableSortLabel active direction="desc">
													Date
												</TableSortLabel>
											</Tooltip>
										</TableCell> */}
                    <TableCell>Email</TableCell>
										<TableCell>Investor</TableCell>
										<TableCell>Capital Amount</TableCell>
										<TableCell>Current Balance</TableCell>
										<TableCell align="right">Details</TableCell>
									</TableRow>
								</TableHead>
								<TableBody>
									{wallets.map((wallet) => (
                   
										<TableRow hover key={wallet._id}>
											<TableCell>{wallet.user? wallet.user.email:""}</TableCell>
											<TableCell>{`${wallet.user? wallet.user.firstName:""} ${wallet.user?wallet.user.lastName:""}`}</TableCell>
											<TableCell>{wallet.capitalAmount}</TableCell>
                      <TableCell>{wallet.currentAmount}</TableCell>
											{/* <TableCell className={classes.totalCell}>
												{wallet.currency} {wallet.value}
											</TableCell> */}
											<TableCell align="right">
												<IconButton
													color="primary"
													component={RouterLink}
													size="small"
													to={`/management/user/details/${wallet._id}`}
													variant="outlined"
												>
													<Forward/>
												</IconButton>
											</TableCell>
										</TableRow>
									))}
								</TableBody>
							</Table>
    )
    // return ( <React.Fragment > {
    //     wallets.map(wallet => <h1>{wallet.toString()}</h1>)
    //   } 
    //   </React.Fragment>)
    };

    export default WalletManagementList;