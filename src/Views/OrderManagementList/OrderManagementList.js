import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/styles';



const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  results: {
    marginTop: theme.spacing(3)
  }
}));

const OrderManagementList = () => {
  const classes = useStyles();
  const [orders, setOrders] = useState([]);

  // useEffect(() => {
  //   let mounted = true;

  //   const fetchOrders = () => {
  //     axios.get('/api/orders').then(response => {
  //       if (mounted) {
  //         setOrders(response.data.orders);
  //       }
  //     });
  //   };

  //   fetchOrders();

  //   return () => {
  //     mounted = false;
  //   };
  // }, []);

  return <h1>Wallets</h1>;
};

export default OrderManagementList;
