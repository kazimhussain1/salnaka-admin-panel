import React, {
  useEffect,
  useState,
  ReactFragment
} from 'react';
import {
  makeStyles
} from '@material-ui/styles';
import { Link as RouterLink } from "react-router-dom";
import clsx from "clsx";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import {Link} from 'react-router-dom';
import {
  Button,
  IconButton,
  Card,
  Grid,
  CardActions,
  CardContent,
  CardHeader,
  Divider,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TableSortLabel,
  Tooltip,
  colors,
} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import axios from '../../utils/axios'
import { Forward } from '@material-ui/icons';



const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  results: {
    marginTop: theme.spacing(3)
  }
}));

const Packages = () => {
  const classes = useStyles();
  const [packages, setPackages] = useState([]);

  useEffect(() => {
    let mounted = true;

    const getPackages = async () => {
      try {
        const response = await axios.get(`${process.env.REACT_APP_AXIOS_BASE_URL}/package`)
        if (mounted) {

          setPackages(response.data.success.packages);
        }
      } catch (error) {
        console.log(error)
      }
    }

    getPackages();

    return () => {
      mounted = false;
    };
  }, []);

  return (
    <React.Fragment>
      <Table>
        <TableHead>
          <TableRow>
            {/* <TableCell sortDirection="desc">
											<Tooltip enterDelay={300} title="Sort">
												<TableSortLabel active direction="desc">
													Date
												</TableSortLabel>
											</Tooltip>
										</TableCell> */}
            <TableCell>Package Name</TableCell>
            <TableCell>Profit Rate</TableCell>
            <TableCell>Profit Range</TableCell>
            <TableCell>Price</TableCell>
            <TableCell>
              <Link to="/management/package">
                <Button
                  variant="contained"
                  color="primary"
                  startIcon={<AddIcon />}
                >
                  Add Package
                </Button>
              </Link>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {packages.map((pkg) => (
            <TableRow hover key={pkg._id}>
              <TableCell>{pkg.name}</TableCell>
              <TableCell>{pkg.profitRate}</TableCell>
              <TableCell>
                {pkg.profitRange ? pkg.profitRange.start : ""}-
                {pkg.profitRange ? pkg.profitRange.end : ""}
              </TableCell>
              <TableCell>{pkg.price}</TableCell>
              {/* <TableCell className={classes.totalCell}>
												{wallet.currency} {wallet.value}
											</TableCell> */}
              <TableCell align="right">
                <IconButton
                  color="primary"
                  component={RouterLink}
                  size="small"
                  to={`/management/package/details/${pkg._id}`}
                  variant="outlined"
                >
                  <Forward />
                </IconButton>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      {/* <Grid container alignItems="center">
        <Grid item xs={12} sm={6}></Grid>
        <Grid item xs={12} sm={6}>
          <Link to="/management/package">
          <Button
            variant="contained"
            color="primary"
            
            startIcon={<AddIcon />}
            
          >
            Add Package
          </Button>
          </Link>
        </Grid>
      </Grid> */}
    </React.Fragment>
  );
  // return ( <React.Fragment > {
  //     wallets.map(wallet => <h1>{wallet.toString()}</h1>)
  //   } 
  //   </React.Fragment>)
};

export default Packages;