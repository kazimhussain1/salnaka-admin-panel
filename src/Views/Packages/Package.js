import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import { Grid } from "@material-ui/core";
import { withRouter } from "react-router-dom";
import axios from "../../utils/axios";
import Page from "../../components/Page";
import Spinner from '../../Layout/Spinner';
import OverView from './Overview';


const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
  },
  container: {
    "& > *": {
      height: "100%",
    },
  },
}));

const PackageDetail = (props) => {
  const classes = useStyles();
  const [packageDetails, setPackageDetails] = React.useState({});
  //let packageDetails={}
  const { match } = props;
  const packageId = match.params.packageId;

  useEffect(() => {
    let mounted = true;
  
    const fetchData = async () => {
      try {
        const response = await axios.get(`/package/${packageId}`);
      
        if (mounted) {
          setPackageDetails(response.data.success.package)
         // packageDetails = response.data.success.package
  
        }

      } catch (error) {
        console.log(error);
      }
     
    };

    fetchData();
    return () => {
      mounted = false;
    };
  }, []);
  
  //   const data = {
    //     customerName: "Azmeer Abrar Khan",
    // 		currAmount: "373,250.50",
    // 		capitalAmount: "123,532.00",
    //   }


  return (
    <Page className={classes.root} title="PackageDetail">
      {packageDetails ? (
        <Grid className={classes.container} container spacing={3}>
          <Grid item xs={12}>
            <OverView data={packageDetails} />
          </Grid>
        
        </Grid>
      ) : (
          <Spinner/>
        )}
    </Page>
  );
};

export default withRouter(PackageDetail);
