import React, { useState, useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import EditIcon from "@material-ui/icons/Edit";
import UpdateIcon from "@material-ui/icons/Update";
import CancelIcon from "@material-ui/icons/Cancel";
import DeleteIcon from "@material-ui/icons/Delete";
import { Redirect, useHistory } from "react-router-dom";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { connect } from "react-redux";
import { setAlert } from "../../actions/alert";

import { makeStyles } from "@material-ui/styles";
import axios from "../../utils/axios";
import PropTypes from "prop-types";
import AddIcon from '@material-ui/icons/Add'
import { red } from "@material-ui/core/colors";
import clsx from "clsx";
//import Slide from "@material-ui/core/Slide";

// const Transition = React.forwardRef(function Transition(props, ref) {
//   return <Slide direction="up" ref={ref} {...props} />;
// });

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: "40px",
  },
  // update: {
  //   display: "none",
  // },
  // updated: {
  //   display: "inline",
  // },
  button: {
    margin: theme.spacing(1),
  },
  // red: {
  //   color: "#FFFFFF",
  //   backgroundColor: red[500],
  //   "&:hover": {
  //     backgroundColor: red[600],
  //   },
  // },
}));

const CreatePackagesForm = ({setAlert}) => {
 

  // const [backupData, setbackUpData] = useState({
  //   Name: "",
  //   Price: "",
  //   ProfitRate: "",
  //   Start: "",
  //   End: "",
  //   Description1: "",
  //   Description2: "",
  //   Description3: "",
  //   Description4: "",
  // });

  const [formData, setFormData] = useState({
    name: "",
    price: "",
    profitRate: "",
    profitRangeStart: "",
    profitRangeEnd: "",
    Description1: "",
    Description2: "",
    Description3: "",
    Description4: "",
  });

     

  const onChange = (e) =>{
    setFormData({ ...formData, [e.target.name]: e.target.value });
  }
  const history = useHistory();

const addPackage = async () => {
  try {
     const data = {
      
       description: [
         formData.Description1,
         formData.Description2,
         formData.Description3,
         formData.Description4,
       ],
       ...formData,
     };
     console.log(data)
    const res = await axios.post(`${process.env.REACT_APP_AXIOS_BASE_URL}/package`,data );
     setAlert('Package Created','success')
    history.goBack()
  } catch (error) {
    error.errors.map(err=>console.log(err.msg))
  }
};

  
  // const [isDisabled, setIsDisabled] = useState(true);
  // const [isHidden, setIsHidden] = useState(true);
  // const [open, setOpen] = useState(false);


  // const handleEditClick = () => {
  //   setIsDisabled(false);
  //   setIsHidden(false);
  // };

  // const handleCancelEditClick = () => {
  //   setFormData(backupData);
  //   setIsDisabled(true);
  //   setIsHidden(true);
  // };

  // const handleUpdate = async () => {
  //   console.log("inside async");
  //   try {
  //     const data = {
  //       id: _id,
  //       description: [
  //         formData.Description1,
  //         formData.Description2,
  //         formData.Description3,
  //         formData.Description4,
  //       ],
  //       ...formData,
  //     };
  //     const res = await axios.put(
  //       `${process.env.REACT_APP_AXIOS_BASE_URL}/package`,
  //       data
  //     );

  //     setIsDisabled(true);
  //   } catch (err) {
  //     console.log(err.response);
  //   }
  // };

  // const handleDialogShow = () => {
  //   setOpen(true);
  // };

  // const handleClose = () => {
  //   setOpen(false);
  // };

  // const handleDelete = async () => {
  //   try {
  //     const result = await axios.delete(
  //       `${process.env.REACT_APP_AXIOS_BASE_URL}/package`,
  //       { data: { packageId: _id } }
  //     );
  //     console.log(result.data.success.msg);
  //     debugger;
  //     setAlert(result.data.success.msg, "warning");
  //     history.goBack();
  //   } catch (error) {
  //     console.log(error.response.data);
  //   }
  // };
  // console.log(open);
  const classes = useStyles();
  return (
    <React.Fragment>
      <Paper elevation={3} variant="outlined" className={classes.paper}>
        <Grid container>
          <Grid item xs={12} sm={4}>
            <Typography variant="h4" gutterBottom>
              Add Package
            </Typography>
          </Grid>

          
        </Grid>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="packageName"
              name="name"
              label="Package name"
              fullWidth
              onChange={(e) => onChange(e)}
              required
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="profitRate"
              name="profitRate"
              label="ProfitRate"
              fullWidth
              onChange={(e) => onChange(e)}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              required
              id="Price"
              name="price"
              label="Price"
              fullWidth
              helperText=" enter in this form eg:xxx-xxx"
              onChange={(e) => onChange(e)}
            />
          </Grid>
          {/* <Typography variant="h4">Profit Range</Typography> */}
          <Grid item xs={12} sm={6}>
            <TextField
              id="start"
              name="profitRangeStart"
              label="Profit Range from"
              fullWidth
              onChange={(e) => onChange(e)}
              required

            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="end"
              name="profitRangeEnd"
              label="Profit Range to"
              fullWidth
              onChange={(e) => onChange(e)}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              required
              id="description"
              onChange={(e) => onChange(e)}
              name="Description1"
              label="first Description"
              fullWidth
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              required
              id="description"
              name="Description2"
              label="second Description"
              fullWidth
              onChange={(e) => onChange(e)}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              required
              id="description"
              name="Description3"
              label="third Description"
              fullWidth
              onChange={(e) => onChange(e)}
            />
          </Grid>

          <Grid item xs={6}>
            <TextField
              required
              id="description"
              name="Description4"
              label="third Description"
              fullWidth
              onChange={(e) => onChange(e)}
            />
          </Grid>
              <Grid item xs={6}>
                <Button
                  variant="contained"
                  onClick={()=>addPackage()}
                  color="primary"
                  startIcon={<AddIcon />}
                >
                  Create
                </Button>
              </Grid>
         
          
        </Grid>
      </Paper>
    </React.Fragment>
  );
};

CreatePackagesForm.propTypes = {
  setAlert: PropTypes.func.isRequired,
  alert: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
  alert: state.alert,
});

export default connect(mapStateToProps, { setAlert })(CreatePackagesForm);
