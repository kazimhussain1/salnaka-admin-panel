import React,{useState,useEffect} from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import EditIcon from '@material-ui/icons/Edit'
import UpdateIcon from '@material-ui/icons/Update';
import CancelIcon from '@material-ui/icons/Cancel';
import DeleteIcon from '@material-ui/icons/Delete';
import {Redirect, useHistory} from 'react-router-dom';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import {connect } from 'react-redux'
import {setAlert} from '../../actions/alert';
import {
  makeStyles
} from '@material-ui/styles';
import axios from '../../utils/axios'
import PropTypes from "prop-types";

import {red} from '@material-ui/core/colors'
import clsx from 'clsx';
import Slide from "@material-ui/core/Slide";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = makeStyles(theme => ({
  paper:{
    padding:'40px'
  },
  update:{
    display:'none',
    
  },
  updated:{
    display:'inline',
      }
  ,
  button:{
    margin:theme.spacing(1)
  },
  red:{
    color: '#FFFFFF',
    backgroundColor: red[500],
    '&:hover':{
      backgroundColor: red[600],
    }
  }
  
}));

const PackagesForm=(props) => {

  const { _id,name,price,profitRange,profitRate,description}=props.data
  const {setAlert}=props
    const [backupData, setbackUpData] = useState({
      Name: "",
      Price: "",
      ProfitRate: "",
      Start: "",
      End: "",
      Description1: "",
      Description2: "",
      Description3: "",
      Description4: "",
    });

    
    const [formData, setFormData] = useState({
      Name: "",
      Price: "",
      ProfitRate: "",
      Start: "",
      End: "",
      Description1: "",
      Description2: "",
      Description3: "",
      Description4: "",
    });


    useEffect(() => {
      setFormData({
       Name: !name ? '' : name,
       Price: !price ? '' : price,
       ProfitRate: !profitRate ? '' : profitRate,
       Start: profitRange ? !profitRange.start ? '' : profitRange.start :'',
       End: profitRange ? !profitRange.end ? '' : profitRange.end : '',
       Description1: description ? !description[0] ? '' : description[0]:'',
       Description2: description ? !description[1] ? '' : description[1] : '',
       Description3: description ? !description[2] ? '' : description[2]:'',
       Description4: description ? !description[3] ? '' : description[3] : '',

      });

      setbackUpData({
        Name: !name ? "" : name,
        Price: !price ? "" : price,
        ProfitRate: !profitRate ? "" : profitRate,
        Start: profitRange ? (!profitRange.start ? "" : profitRange.start) : "",
        End: profitRange ? (!profitRange.end ? "" : profitRange.end) : "",
        Description1: description
          ? !description[0]
            ? ""
            : description[0]
          : "",
        Description2: description
          ? !description[1]
            ? ""
            : description[1]
          : "",
        Description3: description
          ? !description[2]
            ? ""
            : description[2]
          : "",
        Description4: description
          ? !description[3]
            ? ""
            : description[3]
          : "",
      });
    },[name,price,profitRate,profitRange,description])
    

    console.log(formData)
    

   const onChange=e=>setFormData({ ...formData, [e.target.name]: e.target.value})

    const [isDisabled,setIsDisabled]=useState(true);
    const [isHidden, setIsHidden]=useState(true)
    const [open, setOpen] =useState(false);
    const history = useHistory();
    

    const handleEditClick=()=>{
      
      setIsDisabled(false)
      setIsHidden(false)
     }

     const handleCancelEditClick = () => {
       setFormData(backupData)
       setIsDisabled(true);
       setIsHidden(true);
     };

     const handleUpdate= async ()=>{
      console.log("inside async")
        try {
          const data = { id: _id,description: [formData.Description1, formData.Description2, 
            formData.Description3, formData.Description4,],
            name:formData.Name,
            profitRate:formData.ProfitRate,
            price:formData.Price,
            profitRangeStart:formData.Start,
            profitRangeEnd:formData.End,
            
          }
          console.log("send",data);
          const res = await axios.put(`${process.env.REACT_APP_AXIOS_BASE_URL}/package`,data)
          setAlert('Package updated','info')
          setIsDisabled(true)
          setIsHidden(true)
        } catch (err) {
          console.log(err.response)
          const message=err.response.data.errors.map(err=>err.msg)
          setAlert(message,'error')
        }
      
     }
     



    const handleDialogShow=()=>{
     
      setOpen(true)
      
     }

    

    const handleClose=()=>{
      setOpen(false)
    }

    const handleDelete = async ()=>{

      try {
        
        const result = await axios.delete(`${process.env.REACT_APP_AXIOS_BASE_URL}/package`,{data: {packageId: _id}});
        console.log(result.data.success.msg)
        
        setAlert('Package Deleted','error')
        history.goBack();

      } catch (error) {
        console.log(error.response.data)
            const message = error.response.data.errors.map((err) => err.msg);
            setAlert(message, "error");
      }
    }

  const classes = useStyles();
  return (
    <React.Fragment>
      <Dialog
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
        keepMounted
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Package Delete"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Are you sure you want to delete this package?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Disagree
          </Button>
          <Button onClick={handleDelete} color="primary" autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
      <Paper elevation={3} variant="outlined" className={classes.paper}>
        <Grid container>
          <Grid item xs={12} sm={4}>
            <Typography variant="h4" gutterBottom>
              Package Type
            </Typography>
          </Grid>

          <Grid item xs={12} sm={4} md={3}>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={() => handleEditClick()}
              startIcon={<EditIcon />}
            >
              Edit
            </Button>
          </Grid>
          <Grid item xs={12} sm={4} md={3}>
            <Button
              className={clsx(classes.button, classes.red)}
              variant="contained"
              startIcon={<DeleteIcon />}
              onClick={() => handleDialogShow()}
            >
              Delete
            </Button>
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="packageName"
              name="Name"
              label="Package name"
              fullWidth
              value={formData.Name}
              disabled={isDisabled}
              onChange={(e) => onChange(e)}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="profitRate"
              name="ProfitRate"
              label="ProfitRate"
              fullWidth
              disabled={isDisabled}
              value={formData.ProfitRate}
              onChange={(e) => onChange(e)}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              required
              id="Price"
              name="Price"
              label="Price"
              fullWidth
              value={formData.Price}
              disabled={isDisabled}
              onChange={(e) => onChange(e)}
            />
          </Grid>
          {/* <Typography variant="h4">Profit Range</Typography> */}
          <Grid item xs={12} sm={6}>
            <TextField
              id="start"
              name="Start"
              label="Profit Range from"
              fullWidth
              value={formData.Start}
              disabled={isDisabled}
              onChange={(e) => onChange(e)}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="end"
              name="End"
              label="Profit Range to"
              fullWidth
              value={formData.End}
              disabled={isDisabled}
              onChange={(e) => onChange(e)}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              required
              id="description"
              onChange={(e) => onChange(e)}
              disabled={isDisabled}
              name="Description1"
              label="first Description"
              value={formData.Description1}
              fullWidth
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              required
              id="description"
              name="Description2"
              label="second Description"
              fullWidth
              value={formData.Description2}
              disabled={isDisabled}
              onChange={(e) => onChange(e)}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              required
              id="description"
              name="Description3"
              label="third Description"
              fullWidth
              disabled={isDisabled}
              value={formData.Description3}
              onChange={(e) => onChange(e)}
            />
          </Grid>

          <Grid item xs={6}>
            <TextField
              required
              id="description"
              name="Description4"
              label="third Description"
              fullWidth
              disabled={isDisabled}
              value={formData.Description4}
              onChange={(e) => onChange(e)}
            />
          </Grid>
          {!isHidden && (
            <>
              <Grid item xs={6}>
                <Button
                  variant="contained"
                  onClick={() => handleUpdate()}
                  color="primary"
                  startIcon={<UpdateIcon />}
                >
                  Update
                </Button>
              </Grid>
              <Grid item xs={6}>
                <Button
                  variant="contained"
                  className={classes.red}
                  color="primary"
                  onClick={() => handleCancelEditClick()}
                  startIcon={<CancelIcon />}
                >
                  <Typography variant="body">Cancel</Typography>
                </Button>
              </Grid>
            </>
          )}
        </Grid>
      </Paper>
    </React.Fragment>
  );
}


PackagesForm.propTypes = {
  setAlert : PropTypes.func.isRequired,
  alert:PropTypes.array.isRequired,
};

const mapStateToProps = state => ({
  alert:state.alert
})



export default connect(mapStateToProps,{setAlert})(PackagesForm);