import React from "react";
import { Link as RouterLink, Redirect } from "react-router-dom";
import PropTypes from "prop-types";
import {
	Button,
	TableCell,
	TableRow,
	MenuItem,
	TextField,
} from "@material-ui/core";
import * as moment from 'moment';

const allStatus = [
	{
		value: "Pending",
		label: "Pending",
		class: "orange",
	},
	{
		value: "Approved",
		label: "Approved",
		class: "green",
	},
	{
		value: "Denied",
		label: "Denied",
		class: "red",
	},
];

const formatDate = (date) => {
    let formattedDate = moment(date).format('YYYY-MM-DD')
    return formattedDate;
}

export default function Request(props) {
	const { req } = props;

	const [status, setStatus] = React.useState(req.status);
	const [isDisable, setIsDisable] = React.useState(true);
	const [color, setColor] = React.useState("");

	const handleChange = (event) => {
		setStatus(event.target.value);
		event.target.value === "Approved" ? setIsDisable(false) : setIsDisable(true);
	};

	const handleColor = () => {
		if (status == "Pending") setColor("orange");
		else if (status == "Approved") setColor("green");
		else if (status == "Denied") setColor("red");
	};

	const handleUpdate = (event) => {
		const update = {
			transactionId: req._id,
			transactionStatus: status,
		};
        props.onUpdate(update);
	};

	return (
		<TableRow hover>
			<TableCell>{formatDate(req.updatedAt)}</TableCell>
			<TableCell>
				{req.user.email}
			</TableCell>
			<TableCell>
				{req.user.firstName} {req.user.lastName}
			</TableCell>
			<TableCell>{req.user.package.name}</TableCell>
			<TableCell>{req.amount.toFixed(2)}</TableCell>
			<TableCell>
				<TextField
					select
					label=""
					value={status}
					onChange={handleChange}
					size="medium"
					style={{ color: color }}
				>
					{allStatus.map((option) => (
						<MenuItem
							key={option.value}
							value={option.value}
							style={{ color: option.class }}
						>
							{option.value}
						</MenuItem>
					))}
				</TextField>
			</TableCell>
			<TableCell align="right">
				<Button
					color="primary"
					size="small"
					// to={"management/orders/1"}
					variant="outlined"
					onClick={handleUpdate}
				>
					Update
				</Button>
			</TableCell>
		</TableRow>
	);
}
