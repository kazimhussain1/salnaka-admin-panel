import React, { useState, useEffect } from "react";
import { Link as RouterLink, useHistory } from "react-router-dom";
import { connect } from 'react-redux';
import clsx from "clsx";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/styles";
import {
	Button,
	Card,
	CardActions,
	CardContent,
	CardHeader,
	Divider,
	Table,
	TableBody,
	TableCell,
	TableHead,
	TableRow,
	TableSortLabel,
    Tooltip,
    Typography
} from "@material-ui/core";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";

import GenericMoreButton from "../../components/GenericMoreButton";
import { Grid } from "@material-ui/core";
import Page from "../../components/Page";
import Request from "./request";
import {getWithdrawalReqs,updateWithdrawalReq} from '../../actions/withdrawal-req';
import Spinner from '../../Layout/Spinner';

const useStyles = makeStyles((theme) => ({
    root: {
		padding: theme.spacing(3),
	},
	container: {
		"& > *": {
			height: "100%",
		},
	},
	content: {
		padding: 0,
	},
	inner: {
		minWidth: 700,
	},
	progressContainer: {
		padding: theme.spacing(3),
		display: "flex",
		justifyContent: "center",
	},
	actions: {
		justifyContent: "flex-end",
	},
	arrowForwardIcon: {
		marginLeft: theme.spacing(1),
	},
}));


const WithdrwalRequests = (props) => {
	const { className,allStates,getWithdrawalReqs,updateWithdrawalReq, ...rest } = props;

	const classes = useStyles();
    const { loading,requests } = allStates
	

	useEffect(() => {
        getWithdrawalReqs();

		return () => {};
    }, []);

	return (
        <Page className={classes.root} title="Analytics Dashboard">
            <Grid className={classes.container} container spacing={3}>
                <Grid item lg={12} xs={12}>
                    <Card {...rest} className={clsx(classes.root, className)}>
                        <CardHeader 
                            // action={<GenericMoreButton />} 
                            title="Withdrawal Requests" />
                        <Divider />
                        <CardContent className={classes.content}>
                            <PerfectScrollbar options={{ suppressScrollY: true, suppressScrollX: true }} component="div">
                                <div className={classes.inner}>
                                    {loading && requests.length == 0 ?
                                    <Spinner/>: null
                                    }
                                    {(requests && requests.length) ? (
                                        <Table>
                                            <TableHead>
                                                <TableRow>
                                                    <TableCell sortDirection="desc">
                                                        <Tooltip enterDelay={300} title="Sort">
                                                            <TableSortLabel active direction="desc">
                                                                Date
                                                            </TableSortLabel>
                                                        </Tooltip>
                                                    </TableCell>
                                                    <TableCell>Email</TableCell>
                                                    <TableCell>Name</TableCell>
                                                    <TableCell>Package</TableCell>
                                                    <TableCell>Amount</TableCell>
                                                    <TableCell>Status</TableCell>
                                                    <TableCell align="right">Actions</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {requests.map((req) => (
                                                    <Request key={req._id} req={req} onUpdate={updateWithdrawalReq} />
                                                ))}
                                            </TableBody>
                                        </Table>
                                    ):
                                       null
                                    }
                                </div>
                            </PerfectScrollbar>
                        </CardContent>
                        <CardActions className={classes.actions}>
                            {/* <Button
                                color="primary"
                                component={RouterLink}
                                size="small"
                                to="management/orders"
                                variant="text"
                            >
                                See all
                                <ArrowForwardIcon className={classes.arrowForwardIcon} />
                            </Button> */}
                        </CardActions>
                    </Card>
                </Grid>
            </Grid>
        </Page>
	);
};

WithdrwalRequests.propTypes = {
	className: PropTypes.string,
};

const mapStateToProps = state => ({
    allStates: state.withdrawalReq
});

export default connect(mapStateToProps, { getWithdrawalReqs,updateWithdrawalReq })(WithdrwalRequests);