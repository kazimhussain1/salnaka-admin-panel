import React from "react";
import PropTypes from "prop-types";
import NumberFormat from "react-number-format";
import TextField from "@material-ui/core/TextField";

function NumberFormatCustom(props) {
	const { inputRef, onChange, ...other } = props;

	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={(values) => {
				onChange({
					target: {
						name: props.name,
						value: values.value,
					},
				});
			}}
			thousandSeparator
			isNumericString
			prefix="Rs "
		/>
	);
}

NumberFormatCustom.propTypes = {
	inputRef: PropTypes.func.isRequired,
	name: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired,
};

export default function CapitalInput({amount,setAmount,isDisable}) {

	const handleChange = (event) => {
		setAmount(event.target.value);
	};

	return (
		<TextField
			label=""
      value={amount}
			onChange={handleChange}
			name="numberformat"
			disabled={isDisable}
			InputProps={{
				inputComponent: NumberFormatCustom,
			}}
		/>
	);
}
