import React from "react";
import { Link as RouterLink, Redirect } from "react-router-dom";
import PropTypes from "prop-types";
import NumberFormat from "react-number-format";
import {
	Button,
	Card,
	CardActions,
	CardContent,
	CardHeader,
	Divider,
	Table,
	TableBody,
	TableCell,
	TableHead,
	TableRow,
	TableSortLabel,
	Tooltip,
	colors,
	MenuItem,
	TextField,
} from "@material-ui/core";

import CapitalInput from "../CapitalInput/CapitalInput";
import axios from "axios";

const packages = [
	{
		value: "Pending",
		label: "Pending",
		class: "orange",
	},
	{
		value: "Approved",
		label: "Approved",
		class: "green",
	},
	{
		value: "Denied",
		label: "Denied",
		class: "red",
	},
];

export default function Package(props) {
	const { order } = props;

	const [Package, setPackage] = React.useState(order.packageStatus);
	const [amount, setAmount] = React.useState("");
	const [isDisable, setIsDisable] = React.useState(true);
	const [color, setColor] = React.useState("");

	const handleChange = (event) => {
		setPackage(event.target.value);
		event.target.value === "Approved" ? setIsDisable(false) : setIsDisable(true);
	};

	const handleColor = () => {
		if (Package == "Pending") setColor("orange");
		else if (Package == "Approved") setColor("green");
		else if (Package == "Denied") setColor("red");
	};

	const handleUpdate = (event) => {
		const update = {
			userId: order._id,
			capitalAmount: amount,
			packageStatus: Package,
		};
		const body = update;


		// setAuthToken(localStorage.token);
		axios
			.put(`${process.env.REACT_APP_AXIOS_BASE_URL}/updateUser`, body, {
				headers: {
					authorization: `Bearer ${localStorage.token}`,
				},
			})
			.then(() => {
				props.onUpdate();
			});
	};

	return (
		<TableRow hover key={order._id}>
			<TableCell>{order.orderDate}</TableCell>
			<TableCell>
				{order.email}
			</TableCell>
			<TableCell>
				{order.firstName} {order.lastName}
			</TableCell>
			<TableCell>{order.package.name}</TableCell>
			<TableCell>
				<CapitalInput
					amount={amount}
					setAmount={setAmount}
					isDisable={isDisable}
				/>
			</TableCell>
			<TableCell>
				<TextField
					select
					label=""
					value={Package}
					onChange={handleChange}
					size="medium"
					style={{ color: color }}
				>
					{packages.map((option) => (
						<MenuItem
							key={option.value}
							value={option.value}
							style={{ color: option.class }}
						>
							{option.value}
						</MenuItem>
					))}
				</TextField>
			</TableCell>
			<TableCell align="right">
				<Button
					color="primary"
					size="small"
					to={"management/orders/1"}
					variant="outlined"
					onClick={handleUpdate}
				>
					Update
				</Button>
			</TableCell>
		</TableRow>
	);
}
