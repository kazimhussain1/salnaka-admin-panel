import React, { useState, useEffect } from "react";
import { Link as RouterLink, useHistory } from "react-router-dom";
import clsx from "clsx";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/styles";
import {
	Button,
	Card,
	CardActions,
	CardContent,
	CardHeader,
	Divider,
	Table,
	TableBody,
	TableCell,
	TableHead,
	TableRow,
	TableSortLabel,
	Tooltip,
	colors,
} from "@material-ui/core";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";

// import axios from "../../../../utils/axios";
import axios from "axios";
import setAuthToken from "../../../../utils/setAuthToken";
import Label from "../../../../components/Label";
import GenericMoreButton from "../../../../components/GenericMoreButton";
import Package from "../Package/Package";

const useStyles = makeStyles((theme) => ({
	root: {},
	content: {
		padding: 0,
	},
	inner: {
		minWidth: 700,
	},
	progressContainer: {
		padding: theme.spacing(3),
		display: "flex",
		justifyContent: "center",
	},
	actions: {
		justifyContent: "flex-end",
	},
	arrowForwardIcon: {
		marginLeft: theme.spacing(1),
	},
}));

const labelColors = {
	Complete: colors.green[600],
	Pending: colors.orange[600],
	Rejected: colors.red[600],
};

const LatestOrders = (props) => {
	const { className, ...rest } = props;

	const classes = useStyles();
	const [orders, setOrders] = useState(null);

	

	useEffect(() => {
		let mounted = true;

		const fetchOrders = () => {
			setAuthToken(localStorage.token);
			axios
				.get(`${process.env.REACT_APP_AXIOS_BASE_URL}/pendingStatus`, {
					headers: {
						authorization: `Bearer ${localStorage.token}`,
					},
				})
				.then((response) => {
					if (mounted) {
						setOrders(response.data.success);
					}
				});
		};

		fetchOrders();

		return () => {
			mounted = false;
		};
	}, []);

	const onOrderUpdate = ()=>{
		axios
		.get(`${process.env.REACT_APP_AXIOS_BASE_URL}/pendingStatus`, {
			headers: {
				authorization: `Bearer ${localStorage.token}`,
			},
		})
		.then((response) => {
			
			setOrders(response.data.success);
			
		});
	}

	return (
		<Card {...rest} className={clsx(classes.root, className)}>
			<CardHeader action={<GenericMoreButton />} title="Latest Orders" />
			<Divider />
			<CardContent className={classes.content}>
				<PerfectScrollbar options={{ suppressScrollY: true, suppressScrollX: true }} component="div">
					<div className={classes.inner}>
						{orders && (
							<Table>
								<TableHead>
									<TableRow>
										<TableCell sortDirection="desc">
											<Tooltip enterDelay={300} title="Sort">
												<TableSortLabel active direction="desc">
													Date
												</TableSortLabel>
											</Tooltip>
										</TableCell>
										<TableCell>Email</TableCell>
										<TableCell>Name</TableCell>
										<TableCell>Package</TableCell>
										<TableCell>Amount</TableCell>
										<TableCell>Status</TableCell>
										<TableCell align="right">Actions</TableCell>
									</TableRow>
								</TableHead>
								<TableBody>
									{orders.map((order) => (
										<Package order={order} onUpdate={onOrderUpdate} />
									))}
								</TableBody>
							</Table>
						)}
					</div>
				</PerfectScrollbar>
			</CardContent>
			<CardActions className={classes.actions}>
				<Button
					color="primary"
					component={RouterLink}
					size="small"
					to="management/orders"
					variant="text"
				>
					See all
					<ArrowForwardIcon className={classes.arrowForwardIcon} />
				</Button>
			</CardActions>
		</Card>
	);
};

LatestOrders.propTypes = {
	className: PropTypes.string,
};

export default LatestOrders;
