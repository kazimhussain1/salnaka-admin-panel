import React from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import { makeStyles } from "@material-ui/styles";
import { Card, Typography, Grid, colors } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
	root: {},
	content: {
		padding: 0,
	},
	item: {
		padding: theme.spacing(3),
		textAlign: "center",
		[theme.breakpoints.up("md")]: {
			"&:not(:last-of-type)": {
				borderRight: `1px solid ${theme.palette.divider}`,
			},
		},
		[theme.breakpoints.down("sm")]: {
			"&:not(:last-of-type)": {
				borderBottom: `1px solid ${theme.palette.divider}`,
			},
		},
	},
	valueContainer: {
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
	},
	label: {
		marginLeft: theme.spacing(1),
	},
}));

const Overview = (props) => {
	const { className, data } = props;
	const classes = useStyles();

	return (
		<Card className={clsx(classes.root, className)}>
			<Grid alignItems="center" container justify="space-between">
				<Grid className={classes.item} item md={3} xs={12}>
					<Typography component="h2" gutterBottom variant="overline" >
						Customer Name
					</Typography>
					<div className={classes.valueContainer}>
						<Typography variant="h3" >{`${data.user.firstName} ${data.user.lastName}`}</Typography>
					</div>
				</Grid>
				<Grid className={classes.item} item md={3} xs={12}>
					<Typography component="h2" gutterBottom variant="overline" >
						Current Amount
					</Typography>
					<div className={classes.valueContainer}>
						<Typography variant="h3" >Rs {data.currentAmount.toFixed(2)}</Typography>
					</div>
				</Grid>
				<Grid className={classes.item} item md={3} xs={12}>
					<Typography component="h2" gutterBottom variant="overline" >
						Capital Amount
					</Typography>
					<div className={classes.valueContainer}>
						<Typography variant="h3" >Rs {data.capitalAmount}</Typography>
					</div>
				</Grid>
				<Grid className={classes.item} item md={3} xs={12}>
					<Typography component="h2" gutterBottom variant="overline" >
						Referral Code
					</Typography>
					<div className={classes.valueContainer}>
						<Typography variant="h3" >{data.user.referralCode || 'N/A'}</Typography>
					</div>
				</Grid>
			</Grid>
		</Card>
	);
};

Overview.propTypes = {
	className: PropTypes.string,
};

export default Overview;
