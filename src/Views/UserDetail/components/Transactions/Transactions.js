import React, { useState, useEffect } from "react";
import { Link as RouterLink } from "react-router-dom";
import clsx from "clsx";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/styles";
import {
	Button,
	Card,
	CardActions,
	CardContent,
	CardHeader,
	Divider,
	Table,
	TableBody,
	TableCell,
	TableHead,
	TableRow,
	TableSortLabel,
	Tooltip,
	colors,
} from "@material-ui/core";
import { green, red } from '@material-ui/core/colors'
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";

import axios from "../../../../utils/axios";
import Label from "../../../../components/Label";
import GenericMoreButton from "../../../../components/GenericMoreButton";

const useStyles = makeStyles((theme) => ({
	root: {},
	content: {
		padding: 0,
	},
	inner: {
		minWidth: 700,
	},
	progressContainer: {
		padding: theme.spacing(3),
		display: "flex",
		justifyContent: "center",
	},
	actions: {
		justifyContent: "flex-end",
	},
	arrowForwardIcon: {
		marginLeft: theme.spacing(1),
	},
	greenText:{
		color: green[500]
	},
	redText:{
		color: red[500]
	}
}));

const labelColors = {
	complete: colors.green[600],
	pending: colors.orange[600],
	rejected: colors.red[600],
};

const Transactions = (props) => {
	const { transactions } = props;

	const classes = useStyles();
	// const [transactions, setTransactions] = useState(null);

	// useEffect(() => {
	// 	let mounted = true;

	// 	const fetchTransactions = () => {
	// 		axios
	// 			.get(`${process.env.REACT_APP_AXIOS_BASE_URL}/wallet/${userId}`, {
	// 				headers: {
	// 					authorization: `Bearer ${localStorage.token}`,
	// 				},
	// 			})
	// 			.then((response) => {
	// 				if (mounted) {
	// 					setTransactions(response.data.success);
	// 					setOverviewData({
	// 						customerName: "Azmeer Abrar Khan",
	// 						currAmount: "373,250.50",
	// 						capitalAmount: "123,532.00",
	// 					});
	// 				}
	// 			});
	// 	};

	// 	fetchTransactions();

	// 	return () => {
	// 		mounted = false;
	// 	};
	// }, []);

	return (
		<Card className={clsx(classes.root)}>
			<CardHeader action={<GenericMoreButton />} title="Transactions" />
			<Divider />
			<CardContent className={classes.content}>
				<PerfectScrollbar options={{ suppressScrollY: true }}>
					<div className={classes.inner}>
						{transactions && (
							<Table>
								<TableHead>
									<TableRow>
										<TableCell sortDirection="desc">
											<Tooltip enterDelay={300} title="Sort">
												<TableSortLabel active direction="desc">
													Date
												</TableSortLabel>
											</Tooltip>
										</TableCell>
										<TableCell>Description</TableCell>
										<TableCell>Incoming/Outgoing</TableCell>
										<TableCell>Amount</TableCell>
										<TableCell align="right">Actions</TableCell>
									</TableRow>
								</TableHead>
								<TableBody>
									{transactions.map((transaction,ind) => (
										<TableRow hover key={ind}>
											<TableCell>{transaction.date}</TableCell>
											<TableCell>{transaction.description}</TableCell>
											<TableCell className={transaction.action==='Incoming'?classes.greenText:classes.redText}>{transaction.action}</TableCell>
											<TableCell className={classes.totalCell}>
												Rs. {transaction.amount.toFixed(2)}
											</TableCell>
											{/* <TableCell>
												<Label
													color={labelColors[transaction.status]}
													variant="outlined"
												>
													{transaction.status}
												</Label>
											</TableCell> */}
											<TableCell align="right"><GenericMoreButton /></TableCell>
										</TableRow>
									))}
								</TableBody>
							</Table>
						)}
					</div>
				</PerfectScrollbar>
			</CardContent>
		</Card>
	);
};

Transactions.propTypes = {
	className: PropTypes.string,
};

export default Transactions;
