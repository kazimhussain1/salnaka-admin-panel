import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import { Grid } from "@material-ui/core";
import { withRouter } from "react-router-dom";
import axios from "../../utils/axios";
import Page from "../../components/Page";
import { Overview, Transactions } from "./components";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
  },
  container: {
    "& > *": {
      height: "100%",
    },
  },
}));

const WalletDetail = (props) => {
  const classes = useStyles();
  const [walletDetails, setWalletDetails] = React.useState(null);

  const { match } = props;
  const walletId = match.params.userId;

  useEffect(() => {
    let mounted = true;

    const fetchData = async () => {
      try {
        const response = await axios.get(`/wallet/${walletId}`);
        if (mounted) {
          setWalletDetails(response.data.success);
        }
      } catch (error) {
        console.log(error);
      }
    };

    fetchData();

    return () => {
      mounted = false;
    };
  }, []);

  //   const data = {
  //     customerName: "Azmeer Abrar Khan",
  // 		currAmount: "373,250.50",
  // 		capitalAmount: "123,532.00",
  //   }

  return (
    <Page className={classes.root} title="WalletDetail">
      {walletDetails ? (
        <Grid className={classes.container} container spacing={3}>
          <Grid item xs={12}>
            <Overview
              data={{
                ...walletDetails.wallet,
                user: walletDetails.wallet.user,
              }}
            />
          </Grid>
          <Grid item lg={12} xs={12}>
            <Transactions transactions={walletDetails.transactions} />
          </Grid>
        </Grid>
      ) : (
        <h1>Loading</h1>
      )}
    </Page>
  );
};

export default withRouter(WalletDetail);
