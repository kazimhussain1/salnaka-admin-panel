import axios from "axios";

// axios.defaults.baseURL = process.env.REACT_APP_AXIOS_BASE_URL;
// axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem("token");

const instance = axios.create({
  baseURL: process.env.REACT_APP_AXIOS_BASE_URL,
  timeout: 15000,
  headers: { Authorization: "Bearer " + localStorage.getItem("token") },
});

export default instance;
