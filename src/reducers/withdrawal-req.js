import {
    GET_WITHDRAWAL_REQ,
    UPDATE_WITHDRAWAL_REQ,
    WITHDRAWAL_REQ_ERROR,
} from '../actions/types';

const initialState = {
    requests: [],
    req: null,
    loading: true,
    error: {}
}

export default function (state = initialState, action) {
    const { type, payload } = action;

    switch (type) {
        case GET_WITHDRAWAL_REQ:
            return {
                ...state,
                requests: payload,
                loading: false
            }
        case UPDATE_WITHDRAWAL_REQ:
            return {
                ...state,
                // requests: payload,
                // loading: false
            }
        case WITHDRAWAL_REQ_ERROR:
            return {
                ...state,
                error: payload,
                loading: false
            }
        default:
            return state
    }
}