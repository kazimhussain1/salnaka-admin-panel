import { combineReducers } from 'redux';
import alert from './alert';
import auth from './auth';
import profile from './profile';
import post from './post';
import withdrawalReq from './withdrawal-req';

export default combineReducers({
    alert,
    auth,
    profile,
    post,
    withdrawalReq,

});