import axios from 'axios'
import { setAlert } from './alert'
import {
    GET_WITHDRAWAL_REQ,
    UPDATE_WITHDRAWAL_REQ,
    WITHDRAWAL_REQ_ERROR,
} from './types'

const base_url = process.env.REACT_APP_AXIOS_BASE_URL;

// Get Withdrawal req
export const getWithdrawalReqs = () => async dispatch => {
    try {
        const res = await axios.get(`${base_url}/pendingTransactions`);

        dispatch({
            type: GET_WITHDRAWAL_REQ,
            payload: res.data.success.transactions
        })
    } catch (err) {
        let errMsg = '',status = ''
        if(err && err.response){
            errMsg = err.response.statusText
            status = err.response.status
        }else{
            errMsg = 'Network Connection Error'
            status = '500'
        }
        dispatch({
            type: WITHDRAWAL_REQ_ERROR,
            payload: { msg: errMsg, status: status }
        });
    }
}

// update withdrawal req
export const updateWithdrawalReq = (formData) => async dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json',
            authorization: `Bearer ${localStorage.token}`,
        }
    }

    try {
        const res = await axios.post(`${base_url}/updateTransactionStatus`, formData, config);

        dispatch({
            type: UPDATE_WITHDRAWAL_REQ,
            // payload: []
        })

        dispatch(setAlert('Withdrawal Req Updated', 'success'));
        dispatch(getWithdrawalReqs());
    } catch (err) {
        let errMsg = '',status = ''
        if(err && err.response){
            errMsg = err.response.statusText
            status = err.response.status
        }else{
            errMsg = 'Network Connection Error'
            status = '500'
        }
        dispatch({
            type: WITHDRAWAL_REQ_ERROR,
            payload: { msg: errMsg, status: status }
        });
    }
}

