import React, { Fragment, useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Auth from "./Layout/Auth";
import Login from "./Views/Login";
import Routes from "./components/routing/Roustes";

import { ThemeProvider } from "@material-ui/styles";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import MomentUtils from "@date-io/moment";
import { SnackbarProvider } from "notistack";
// Redux
import { Provider } from "react-redux";
import store from "./store";
import { loadUser } from "./actions/auth";
import setAuthToken from "./utils/setAuthToken";

import theme from "./theme";
import "./App.css";

import Dashboard from "./Layout/Dashboard";
import Overview from "./Views/Overview"
import WalletList from "./Views/UserList"

import Error from "./Layout/Error";
import ErrView from "./Views/Error404"
import Alert from './pages/Alert';

import WalletDetail from "./Views/UserDetail";
import Packages from './Views/Packages';
import PrivateRoute from "./components/routing/PrivateRoute";
import WithdrwalRequests from "./Views/WithdrawalRequests";
import Package from './Views/Packages/Package';
import CreatePackage from './Views/Packages/addPackage'

if (localStorage.token) {
	setAuthToken(localStorage.token);
}

const App = () => {
	useEffect(() => {
		store.dispatch(loadUser());
	}, []);

	return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <MuiPickersUtilsProvider utils={MomentUtils}>
          <SnackbarProvider>
            <Router>
              <Fragment>
                <Alert />
                <Switch>
                  <PrivateRoute
                    exact
                    path="/dashboard"
                    component={Dashboard}
                    view={Overview}
                  />
                  <PrivateRoute
                    exact
                    path="/management/users"
                    component={Dashboard}
                    view={WalletList}
                  />
                  <PrivateRoute
                    exact
                    path="/management/user/details/:userId"
                    component={Dashboard}
                    view={WalletDetail}
                  />
                  <PrivateRoute
                    exact
                    path="/management/withdrawal-requests"
                    component={Dashboard}
                    view={WithdrwalRequests}
                  />
                  <PrivateRoute
                    exact
                    path="/management/packages"
                    component={Dashboard}
                    view={Packages}
                  />
                  <PrivateRoute
                    exact
                    path="/management/package/details/:packageId"
                    component={Dashboard}
                    view={Package}
                  />
                  <PrivateRoute
                    exact
                    path="/management/package"
                    component={Dashboard}
                    view={CreatePackage}
                  />
                  <Route exact path="/" render={() => <Auth view={Login} />} />
                  <Route render={() => <Error view={ErrView} />} />
                </Switch>
              </Fragment>
            </Router>
          </SnackbarProvider>
        </MuiPickersUtilsProvider>
      </ThemeProvider>
    </Provider>
  );
};

export default App;
